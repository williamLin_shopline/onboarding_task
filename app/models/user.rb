class User
  include Mongoid::Document
  field :first_name, type: String
  field :last_name, type: String
  field :age, type: Integer
  field :gender, type: String
  field :address, type: Hash, default: { county: 'county', address_1: 'address_1', address_2: 'address_2' }

  validates :last_name, presence: true
  validates :first_name, presence: true
  validates :age, numericality: { greater_than: 0 }
  validates :gender, inclusion: { in: %w(male female others) }

  has_one :shop


  def name
  	"#{last_name} #{first_name}"
  end
end
